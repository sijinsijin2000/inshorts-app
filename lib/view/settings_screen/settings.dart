// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:inshorts/controller/settings.dart';
import 'package:inshorts/global/global.dart';
import 'package:inshorts/style/colors.dart';
import 'package:inshorts/style/text_style.dart';
import '../../aplication_localization.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2,
        title: Text(
          AppLocalizations.of(context).translate('settings'),
          style: AppTextStyle.appBarTitle.copyWith(
            fontSize: 18,
            color: Provider.of<SettingsProvider>(context, listen: false)
                    .isDarkThemeOn
                ? AppColor.background
                : AppColor.onBackground,
          ),
        ),
      ),
      body: Consumer<SettingsProvider>(
        builder: (context, settingsProvider, child) => Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 25,
              ),
              ListTile(
                leading: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent, shape: BoxShape.circle),
                  height: 50,
                  child: IconButton(
                    icon: Icon(FeatherIcons.sunset),
                    onPressed: null,
                    // splashColor: Colors.pink,
                  ),
                ),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 14.0, horizontal: 16.0),
                // Container(
                //   decoration: BoxDecoration(
                //       color: Colors.lightBlueAccent, shape: BoxShape.circle),
                //   height: 50,
                //  child: IconButton( Icon( FeatherIcons.sunset,
                // ),),),
                title:
                    Text(AppLocalizations.of(context).translate('dark_theme')),
                subtitle: Text(AppLocalizations.of(context)
                    .translate('darktheme_message')),
                onTap: null,
                tileColor: Color(0xFFF5F6F9),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                trailing: Switch(
                    activeColor: AppColor.accent,
                    value: settingsProvider.isDarkThemeOn,
                    onChanged: (status) {
                      settingsProvider.darkTheme(status);
                    }),
              ),
              SizedBox(
                height: 25,
              ),
              ListTile(
                leading: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent, shape: BoxShape.circle),
                  height: 50,
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.language),
                    onPressed: null,
                    // splashColor: Colors.pink,
                  ),
                ),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 14.0, horizontal: 16.0),

                title: Text(AppLocalizations.of(context).translate('language')),
                onTap: null,
                tileColor: Color(0xFFF5F6F9),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                // style: ,
                trailing: DropdownButton(
                    underline: Container(),
                    value: settingsProvider.activeLanguge,
                    items: Global.lang.map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    onChanged: (v) {
                      settingsProvider.setLang(v);
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
