import 'package:flutter/material.dart';
import 'package:inshorts/view/choose_language/choose_language.dart';
import 'package:inshorts/view/feed_screen/feed.dart';
import 'package:inshorts/view/login_screen/login_screen.dart';
import 'package:inshorts/view/onboarding_screen/widget/animatedSlideUp.dart';

// This is the best practice

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  'assets/images/onboarding.png',
                  width: 600.0,
                  height: 240.0,
                ),
                const Padding(
                  padding: EdgeInsets.all(25.0),
                  child: Text(
                    'Get the latest updated Articles Easily with Us ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(30, 5, 30, 20),
                  child: Text(
                    'Come on, get the latest updated every day and add insight, your trusted knowledge with us ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                )
              ],
            ),
          ),
          AnimatedSlideUp(),
          GestureDetector(
            onVerticalDragUpdate: (dragUpdateDetails) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => ChooseLangScreen()));
            },
          ),
        ],
      ),
    );
  }
}
