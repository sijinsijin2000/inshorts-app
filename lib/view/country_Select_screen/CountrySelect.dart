import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:inshorts/view/login_screen/login_screen.dart';

// This is the best practice

class CountrySelectScreen extends StatefulWidget {
  @override
  _CountrySelectScreenState createState() => _CountrySelectScreenState();
}

class _CountrySelectScreenState extends State<CountrySelectScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Center(
            child: Column(
              // mainAxisSize: MainAxisSize.min,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 40, 20, 10),
                  child: Text(
                    'Choose your Country ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: GridView(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 16,
                          childAspectRatio: 3,
                          mainAxisSpacing: 16),
                      shrinkWrap: true,
                      children: [
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'UAE',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'UK',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'US',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Phillipines',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Turkey',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Sweden',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Saudi Arabia',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Egypt',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            debugPrint('Received click');
                          },
                          child: const Text(
                            'Germany',
                            // style: TextStyle(fontSize: 20),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    LoginScreen()));
                            debugPrint('Received click');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.asset(
                                "assets/images/flags/icon10.png",
                                width: 25.0,
                                height: 25.0,
                              ),
                              const Text(
                                'Israe',
                                // style: TextStyle(fontSize: 20),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
