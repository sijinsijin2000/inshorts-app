// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:inshorts/controller/feed_controller.dart';
import 'package:inshorts/global/global.dart';
import 'package:inshorts/style/colors.dart';
import 'package:inshorts/style/text_style.dart';

class CategoryCard extends StatelessWidget {
  final String icon;
  final String title;
  final Function onTap;
  final bool active;

  const CategoryCard(
      {Key key, this.icon, this.title, this.onTap, this.active = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
        FeedController.addCurrentPage(1);
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        height: Global.height(context) * 0.15,
        decoration: new BoxDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Opacity(
              opacity: active ? 1 : 0.5,
              child: Image.asset(
                "assets/icons/$icon.png",
                height: 70,
                width: 70,
                fit: BoxFit.contain,
              ),
            ),
            Text(
              title,
              style: active
                  ? AppTextStyle.categoryTitle.copyWith(
                      color: AppColor.accent,
                      fontSize: 20,
                      fontWeight: FontWeight.w500)
                  : AppTextStyle.categoryTitle,
            ),
          ],
        ),
      ),
    );
  }
}
