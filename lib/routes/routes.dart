// Package imports:
import 'package:auto_route/auto_route_annotations.dart';

// Project imports:
import 'package:inshorts/view/app_base/app_base.dart';
import 'package:inshorts/view/bookmarked_screen/bookmark.dart';
import 'package:inshorts/view/discover_screen/discover.dart';
import 'package:inshorts/view/feed_screen/feed.dart';
import 'package:inshorts/view/photo_view/photo_expanded_screen.dart';
import 'package:inshorts/view/search_screen/search.dart';
import 'package:inshorts/view/settings_screen/settings.dart';
import 'package:inshorts/view/web_screen/web.dart';

@autoRouter
class $Router {
  SearchScreen searchScreen;
  SettingsScreen settingsScreen;
  BookmarkScreen bookmarkScreen;
  WebScreen webScreen;
  DiscoverScreen discoverScreen;
  FeedScreen feedScreen;
  ExpandedImageView expandedView;
  @initial
  AppBase appBase;
}
